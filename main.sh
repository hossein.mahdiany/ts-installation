#!/bin/bash

# Enter Traffic OPS information
read -p "Please enter Traffic ops username: " USERNAME
read -p "Please enter Traffic ops password: " PASSWORD
read -p "Please enter Traffic ops url without http(example: ops.com): " URL
read -p "Please enter InfluxDB username: " DBUSER
read -p "Please enter InfluxDB password: " DBPASS
read -p "Please enter Grafana password you want to set: " GRAFANA_PASS
read -p "Please enter Traffic Stats FQDN: " TS_HOSTNAME

# install Go
yum -y install epel-release
yum -y install go

# Add InfluxDB repository and Install it
cp /root/TS-installation/influxdb.repo /etc/yum.repos.d/
yum -y install influxdb 
systemctl enable influxdb; systemctl start influxdb; systemctl status influxdb

# Install Grafana
wget https://dl.grafana.com/oss/release/grafana-7.0.3-1.x86_64.rpm
yum -y localinstall grafana-7.0.3-1.x86_64.rpm

# Configure /etc/grafana/grafana.ini
sed -i "s/;domain = localhost/domain = $TS_HOSTNAME/g" /etc/grafana/grafana.ini
sed -i "s/;admin_user = admin/admin_user = admin/g" /etc/grafana/grafana.ini
sed -i "s/;admin_password = admin/admin_password = $GRAFANA_PASS/g" /etc/grafana/grafana.ini
sed -i "0,/;enabled = false/ s/;enabled = false/enabled = true/" /etc/grafana/grafana.ini

# Enable and start Grafana service
systemctl enable grafana-server; systemctl start grafana-server; systemctl status grafana-server

# Install Traffic Stats
#wget https://store1.khallagh.com/cdn-production/traffic_stats-4.2.0-10778.49f13f94.el7.x86_64.rpm
yum -y localinstall traffic_stats-4.2.0-10778.49f13f94.el7.x86_64.rpm

# Copy configuration file
cp /root/TS-installation/traffic_stats.cfg /opt/traffic_stats/conf/traffic_stats.cfg
sed -i "s/USERNAME/$USERNAME/g" /opt/traffic_stats/conf/traffic_stats.cfg
sed -i "s/PASSWORD/$PASSWORD/g" /opt/traffic_stats/conf/traffic_stats.cfg
sed -i "s/URL/$URL/g" /opt/traffic_stats/conf/traffic_stats.cfg
sed -i "s/DBUSER/$DBUSER/g" /opt/traffic_stats/conf/traffic_stats.cfg
sed -i "s/DBPASS/$DBPASS/g" /opt/traffic_stats/conf/traffic_stats.cfg

# Create database for Traffic Stats
cd /opt/traffic_stats/influxdb_tools/
./create_ts_databases --user $DBUSER --password $DBPASS

# Enable and start Traffic Stats service
systemctl enable traffic_stats; systemctl start traffic_stats; systemctl status traffic_stats

